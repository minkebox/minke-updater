FROM alpine:latest

RUN apk add docker-cli

COPY root/ /

LABEL net.minkebox.system="true"

ENTRYPOINT ["/startup.sh"]
