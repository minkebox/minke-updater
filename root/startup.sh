#! /bin/sh
#
# CMD
# ID

# Wait for container ID to terminate
while [ "$(docker container ls -q | grep ${ID})" != "" ]; do
  sleep 1
done

docker container rm ${ID}
echo ${CMD}
docker run ${CMD}
